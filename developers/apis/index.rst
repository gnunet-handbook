Service APIs
============

These services comprise a backbone of core services for 
peer-to-peer applications to use.

.. toctree::
   cadet.rst
   core.rst
   cong.rst
   dht.rst
   fs.rst
   gns.rst
   identity.rst
   messenger.rst
   namecache.rst
   namestore.rst
   nse.rst
   peerstore.rst
   rest.rst
   revocation.rst
   setops.rst
   statistics.rst
   transport.rst


