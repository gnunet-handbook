Specifications
==============

On this page you can find links to our RFC-style technical protocol
specifications. We have dubbed our specifications "Living Standards Documents (LSDs)".
Some LSDs have at some point become RFCs, in which case you can find the link
to the RFC below.
RFCs, once published, are not changed.
LSDs, however, "live" and always represent the current status of the implemented
protocol. The RFC in such cases only represents a snapshot in this case which
may or may not represent the current status of the corresponding LSD.

-   LSD 0000: Reserved
-  `LSD 0001 <https://lsd.gnunet.org/lsd0001>`__ [`RFC 9498 <https://www.rfc-editor.org/rfc/rfc9498.html>`__]: **The GNU Name System**
-  `LSD 0002 <https://lsd.gnunet.org/lsd0002>`__: **re:claimID**
-  `LSD 0003 <https://lsd.gnunet.org/lsd0003>`__: **Byzantine Fault Tolerant Set Reconciliation**
-  `LSD 0004 <https://lsd.gnunet.org/lsd0004>`__: **The R5N Distributed Hash Table**
-  `LSD 0005 <https://lsd.gnunet.org/lsd0005>`__: **The GNU Name System DID Method**
-  `LSD 0006 <https://lsd.gnunet.org/lsd0006>`__: **The 'taler' URI scheme for GNU Taler Wallet interactions**
-  `LSD 0007 <https://lsd.gnunet.org/lsd0007>`__: **The GNUnet communicators**
